package main

import (
	httpPackage "chat/http"
	hubPackage "chat/hub"
	websocketPackage "chat/websocket"
	"log"
	"net/http"
	"os"
)

func main() {
	hub := hubPackage.NewHub()
	go hub.Start()

	http.Handle("/", httpPackage.NewPageHandler())
	http.Handle("/websocket", websocketPackage.NewHandler(&hub))

	port := os.Getenv("CHAT_PORT")
	if port == "" {
		port = "8080"
	}
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatal("Can not serve http requests", err)
	}
}
