FROM golang:1.15.6
COPY . /go/src/chat
WORKDIR /go/src/chat
RUN CGO_ENABLED=0 go install -v ./...
CMD ["/go/bin/chat"]
