package rpc

type Envelope struct {
	Method string `json:"method"`
}

type BroadcastMethod struct {
	Message string `json:"message"`
}

type SetNameMethod struct {
	Name string `json:"name"`
}
