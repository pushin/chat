package hub

import (
	clientPackage "chat/client"
	"chat/client/rpc"
	messagePackage "chat/message"
	"chat/user"
	"log"
)

type Hub struct {
	clients           [5]*clientPackage.Client
	RegisterChannel   chan *clientPackage.Client
	UnRegisterChannel chan *clientPackage.Client
	MessageChannel    chan messagePackage.Message
}

func (h *Hub) IsRegisteredClient(client *clientPackage.Client) bool {
	for _, c := range h.clients {
		if c == client {
			return true
		}
	}

	return false
}

func (h *Hub) registerClient(client *clientPackage.Client) error {
	for index, c := range h.clients {
		if c == nil {
			h.clients[index] = client
			h.SendUserList()
			return nil
		}
	}

	return ClientPoolOverflowError{message: "Client pool overflow"}
}

func (h *Hub) unRegisterClient(client *clientPackage.Client) {
	for index, c := range h.clients {
		if c == client {
			h.clients[index] = nil
			h.SendUserList()
			return
		}
	}
}

func (h *Hub) buildUserList() []user.User {
	var list []user.User
	for _, client := range h.clients {
		if client == nil {
			continue
		}
		list = append(list, user.User{Name: client.Name})
	}
	return list
}

func (h *Hub) SendUserList() {
	for _, client := range h.clients {
		if client == nil {
			continue
		}
		client.Rpc(rpc.NewUpdateUserListMethod(h.buildUserList()))
	}
}

func (h *Hub) broadcastMessage(message messagePackage.Message) {
	for _, client := range h.clients {
		if client == nil {
			continue
		}
		client.Rpc(rpc.NewBroadcastMethod(message))
	}
}

func (h *Hub) Start() {
	for {
		select {
		case client := <-h.RegisterChannel:
			err := h.registerClient(client)
			if err != nil {
				client.Rpc(rpc.NewErrorMethod("Чат переполнен"))
				log.Println(err)
			}
		case client := <-h.UnRegisterChannel:
			h.unRegisterClient(client)
		case message := <-h.MessageChannel:
			h.broadcastMessage(message)
		}
	}
}

func NewHub() Hub {
	return Hub{
		RegisterChannel:   make(chan *clientPackage.Client),
		UnRegisterChannel: make(chan *clientPackage.Client),
		MessageChannel:    make(chan messagePackage.Message),
	}
}

type ClientPoolOverflowError struct {
	message string
}

func (e ClientPoolOverflowError) Error() string { return e.message }
