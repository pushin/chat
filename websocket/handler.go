package websocket

import (
	clientPackage "chat/client"
	hubPackage "chat/hub"
	hubRpc "chat/hub/rpc"
	"chat/websocket/controllers"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

func NewHandler(hub *hubPackage.Hub) Handler {
	return Handler{
		httpToWebsocketUpgrader: &websocket.Upgrader{},
		hub:                     hub,
		controllers: []Controller{
			controllers.Broadcast{},
			controllers.SetName{},
		},
	}
}

type Handler struct {
	httpToWebsocketUpgrader *websocket.Upgrader
	hub                     *hubPackage.Hub
	controllers             []Controller
}

func (handler Handler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	conn, err := handler.httpToWebsocketUpgrader.Upgrade(writer, request, nil)
	defer func() {
		if conn == nil {
			return
		}
		if err := conn.Close(); err != nil {
			log.Println(err)
		}
	}()
	if err != nil {
		log.Println("Can not upgrade HTTP connection to WebSocket", err)
		return
	}

	client := clientPackage.NewClient(conn)
	handler.hub.RegisterChannel <- &client

	for {
		messageType, messageText, err := conn.ReadMessage()
		if err != nil {
			log.Println("Can not read message", err)
			handler.hub.UnRegisterChannel <- &client
			break
		}
		if messageType != websocket.TextMessage {
			continue
		}

		if !handler.hub.IsRegisteredClient(&client) {
			continue
		}

		envelope := hubRpc.Envelope{}
		errMessageUnmarshal := json.Unmarshal(messageText, &envelope)
		if errMessageUnmarshal != nil {
			panic(errMessageUnmarshal)
		}

		for _, controller := range handler.controllers {
			if controller.Support(envelope) {
				controller.Execute(messageText, &client, handler.hub)
			}
		}

		fmt.Println(string(messageText), handler.hub)
	}

}
