package controllers

import (
	clientPackage "chat/client"
	clientRpc "chat/client/rpc"
	hubPackage "chat/hub"
	"chat/hub/rpc"
	messagePackage "chat/message"
	"encoding/json"
)

type Broadcast struct {
}

func (b Broadcast) Support(envelope rpc.Envelope) bool {
	return envelope.Method == "broadcast"
}

func (b Broadcast) Execute(messageText []byte, client *clientPackage.Client, hub *hubPackage.Hub) {
	broadcastMethod := rpc.BroadcastMethod{}
	errMessageUnmarshal := json.Unmarshal(messageText, &broadcastMethod)
	if errMessageUnmarshal != nil {
		panic(errMessageUnmarshal)
	}
	message := messagePackage.NewMessage(broadcastMethod.Message)
	message.User = client.Name
	if message.User != "" {
		hub.MessageChannel <- message
	} else {
		client.Rpc(clientRpc.NewErrorMethod("User name can not be empty"))
	}
}
