package controllers

import (
	"chat/client"
	hubPackage "chat/hub"
	hubRpc "chat/hub/rpc"
	"encoding/json"
)

type SetName struct {
}

func (s SetName) Support(envelope hubRpc.Envelope) bool {
	return envelope.Method == "set_name"
}

func (s SetName) Execute(messageText []byte, client *client.Client, hub *hubPackage.Hub) {
	setNameMethod := hubRpc.SetNameMethod{}
	errMessageUnmarshal := json.Unmarshal(messageText, &setNameMethod)
	if errMessageUnmarshal != nil {
		panic(errMessageUnmarshal)
	}
	client.Name = setNameMethod.Name
	hub.SendUserList()
}
