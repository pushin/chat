package websocket

import (
	"chat/client"
	"chat/hub"
	"chat/hub/rpc"
)

type Controller interface {
	Support(envelope rpc.Envelope) bool
	Execute(messageText []byte, client *client.Client, hub *hub.Hub)
}
