package http

import (
	"net/http"
)

func NewPageHandler() PageHandler {
	return PageHandler{}
}

type PageHandler struct {
}

func (h PageHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	http.ServeFile(writer, request, "index.html")
}
