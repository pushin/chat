package message

import "time"

type Message struct {
	User string    `json:"user"`
	Time time.Time `json:"time"`
	Text string    `json:"text"`
}

func NewMessage(text string) Message {
	return Message{
		Time: time.Now(),
		Text: text,
	}
}
