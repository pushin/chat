package client

import (
	"github.com/gorilla/websocket"
	"log"
)

type Method interface {
	Serialize() ([]byte, error)
}

type Client struct {
	Name string
	conn *websocket.Conn
}

func (client Client) Rpc(method Method) {
	if client.conn == nil {
		log.Println("Invalid connection")
		return
	}
	messageJson, jsonErr := method.Serialize()
	if jsonErr != nil {
		log.Println("Can not convert rpc to json", jsonErr)
	}
	err := client.conn.WriteMessage(websocket.TextMessage, messageJson)
	if err != nil {
		log.Println("Can not send rpc to client", err)
	}
}

func NewClient(conn *websocket.Conn) Client {
	return Client{
		conn: conn,
	}
}
