package rpc

import (
	"chat/message"
	"encoding/json"
)

type BroadcastMethod struct {
	Method  string          `json:"method"`
	Message message.Message `json:"message"`
}

func NewBroadcastMethod(message message.Message) BroadcastMethod {
	return BroadcastMethod{Method: "broadcast", Message: message}
}

func (b BroadcastMethod) Serialize() ([]byte, error) {
	return json.Marshal(b)
}
