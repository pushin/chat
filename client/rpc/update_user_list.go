package rpc

import (
	"chat/user"
	"encoding/json"
)

func NewUpdateUserListMethod(list []user.User) UpdateUserListMethod {
	return UpdateUserListMethod{Method: "set_user_list", Users: list}
}

type UpdateUserListMethod struct {
	Method string      `json:"method"`
	Users  []user.User `json:"users"`
}

func (s UpdateUserListMethod) Serialize() ([]byte, error) {
	return json.Marshal(s)
}
