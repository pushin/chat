package rpc

import "encoding/json"

func NewErrorMethod(message string) ErrorMethod {
	return ErrorMethod{Method: "error", Message: message}
}

type ErrorMethod struct {
	Method  string `json:"method"`
	Message string `json:"message"`
}

func (e ErrorMethod) Serialize() ([]byte, error) {
	return json.Marshal(e)
}
